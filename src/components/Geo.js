import { useState } from 'react';
import axios from "axios";

function Geo({getGeoLocation}) {
    const [location, setLocation] = useState(false);

    if (!location) {
        axios.get('https://ipgeolocation.abstractapi.com/v1', {
            params: {
                api_key: process.env.REACT_APP_GEO_API_KEY,
            }
        })
            .then(function(response) {
                if (response.status === 200 && response.data && response.data.city) {
                    setLocation(response.data.city);
                }
            })
            .catch(function(error) {
                console.log(error);
            });
    }

    function handleClick(e) {
        e.preventDefault();
        getGeoLocation(location);
    }

    return location && (
        <p className="geo">Are you located in <a onClick={handleClick} href="#">{location}</a>?</p>
    );
}

export default Geo;
